/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  Image,
  Platform,
  PermissionsAndroid,
  NativeModules,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {launchCamera} from 'react-native-image-picker';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [imageUri, setImageUri] = useState({id: 1, uri: null});
  const [imageData, setImageData] = useState(null);

  const requestCameraPermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message: 'App needs camera permission',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };
  const requestExternalWritePermission = async () => {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            title: 'External storage write permission',
            message: 'App needs external storage write permission',
          },
        );
        return granted === PermissionsAndroid.RESULTS.GRANTED;
      } catch (err) {
        console.warn(err);
        return false;
      }
    } else return true;
  };

  const takePicture = async () => {
    let options = {
      maxWidth: 800,
      maxHeight: 800,
      storageOptions: {
        sklipBackup: true,
        path: 'images',
      },
    };

    let isCameraPermitted = await requestCameraPermission();
    let isStoragePermitted = await requestExternalWritePermission();

    if (isCameraPermitted && isStoragePermitted) {
      launchCamera(options, async response => {
        console.log('Response = ', response);

        if (response.didCancel) {
          console.log('User cancelled image picker');
        } else if (response.error) {
          console.log('Image picker error: ', response.error);
        } else if (response.customButton) {
          console.log('user tapped custom button: ', response.customButton);
        } else {
          if (response.assets.length > 0) {
            console.log('Response URI', response.assets[0].uri);
            setImageUri(prev => {
              return {id: prev.id + 1, uri: response.assets[0].uri};
            });
          }
        }
      });
    }
  };

  const analyze = () => {
    if (imageUri.uri) {
      console.log('Analyze');
      const path = imageUri.uri.replace('file://', '');
      NativeModules.BitmapModule.getPixels(path)
        .then(imageDataRes => {
          console.log('Analyzed');
          setImageData(imageDataRes);
        })
        .catch(err => console.log(err));
    }
  };

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <View style={styles.sectionContainer}>
            <Text>Test application - React Native</Text>
          </View>
          <View style={styles.sectionContainer}>
            <Button onPress={takePicture} title="Take a picture" />
          </View>
          {imageUri && imageUri.uri && (
            <View style={styles.imageContainer}>
              <Image
                style={styles.image}
                source={{uri: `${imageUri.uri}?v=${new Date().toISOString()}`}}
              />
            </View>
          )}
          <View style={styles.sectionContainer}>
            <Button onPress={analyze} title="Analyze picture" />
          </View>
          {imageData && (
            <View style={styles.sectionContainer}>
              <Text>
                Image dimensions: {imageData.width}, {imageData.height}
              </Text>

              <Text>First pixel color: {imageData.pixels[0]}</Text>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  imageContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    marginTop: 20,
    width: 400,
    height: 400,
  },
});

export default App;
